### hello_world

Uses the `tokio` rust crate to handle asynchronous connections. Using `tokio` it will be able to handle multiple concurrent connections with speed and more importantly reliably because of rust's characteristics.

To run just simply clone the repo, `cd` into the `hello_world` folder and `cargo run`. Then in another terminal run `telnet localhost 6142`.

This is the beginning of something very cool!
